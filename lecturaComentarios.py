import numpy as np
import nltk
import pandas as pd
import itertools
import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
from os import path
from PIL import Image

from generales import function_tokenizar

# lectura de archivo
dfComentarios = pd.read_csv('E:\\especializacion\\semestre 2\\trabajo de grado\\nltk\\comentarios.txt', sep=', ', 
                delimiter='\t\t', 
                header='infer', 
                names=None, 
                index_col=None, 
                usecols=None, 
                squeeze=False, 
                engine="python")

#conversion de dataframe a lista
lsComentarios = dfComentarios.values.tolist()

# llamamos la funcion de tokenizacion
rs_tokenizacion = function_tokenizar(lsComentarios)
lsFecuencias = rs_tokenizacion[0]

## generación de frecuencias
freqComentarios = []
freqComentarios  = list(itertools.chain.from_iterable(lsFecuencias))
fd = nltk.FreqDist(freqComentarios)

#plt.subplot(2, 1, 1)
fd.plot(30)

## generando nube de palabras
wordcloud = WordCloud()
wordcloud2 = wordcloud.generate_from_frequencies(frequencies=fd)
# Display the generated image:
plt.imshow(wordcloud2, interpolation='bilinear')
plt.axis("off")
#plt.subplot(2, 1, 2)

plt.show()


#import numpy as np
#import matplotlib.pyplot as plt


#x1 = np.linspace(0.0, 5.0)
#x2 = np.linspace(0.0, 2.0)

#y1 = np.cos(2 * np.pi * x1) * np.exp(-x1)
#y2 = np.cos(2 * np.pi * x2)

#plt.subplot(2, 1, 1)
#plt.plot(x1, y1, 'o-')
#plt.title('A tale of 2 subplots')
#plt.ylabel('Damped oscillation')

#plt.subplot(2, 1, 2)
#plt.plot(x2, y2, '.-')
#plt.xlabel('time (s)')
#plt.ylabel('Undamped')

#plt.show()

#mylist = rs_tokenizacion[0][0]
#x = ' '.join(mylist)
