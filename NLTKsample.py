# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 20:15:37 2019

@author: Estefania
"""

from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')
filterdText=tokenizer.tokenize('Hello Guru99, You 90 have bui$ld % & a very good site and I love visiting your site.')
print(filterdText)

from nltk.tokenize import word_tokenize
text = "God is Great! I won a lottery."
print(word_tokenize(text))

from nltk.tokenize import sent_tokenize
text = "God is Great! I won a lottery."
print(sent_tokenize(text))


## POS (Part-Of-Speech) Tagging & Chunking with NLTK
from nltk import pos_tag
from nltk import RegexpParser
text ="learn php from guru99 and make study easy".split()
print("After Split:",text)
tokens_tag = pos_tag(text)
print("After Token:",tokens_tag)
patterns= """mychunk:{<NN.?>*<VBD.?>*<JJ.?>*<CC>?}"""
chunker = RegexpParser(patterns)
print("After Regex:",chunker)
output = chunker.parse(tokens_tag)
print("After Chunking",output)


import nltk
text = "david estudia ingles, kyra es un perro"
# word_tokenize particiona una cadena de texto en palabras
tokens = nltk.word_tokenize(text)
print(tokens)
## pos_tag le asigna un token a cada una de las palabras
tag = nltk.pos_tag(tokens)
print(tag)
grammar = "NP: {<DT>?<JJ>*<NN>}"
cp  =nltk.RegexpParser(grammar)
result = cp.parse(tag)
print(result)
result.draw()    # It will draw the pattern graphically which can be seen in Noun Phrase chunking 