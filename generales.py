import nltk
import unicodedata
from nltk.tokenize import RegexpTokenizer, word_tokenize, sent_tokenize
from nltk import pos_tag
from nltk.corpus import stopwords
from collections import Counter
import unidecode
stop_words = set(stopwords.words('spanish'))


def function_tokenizar(lsComentarios):
    # limpieza de puntucaciones, caracteres especiales y tokenización
    tokenizer = RegexpTokenizer(r'\w+')
    cantComentarios = len(lsComentarios)
    lsLimpiaComentarios = []
    lsComentariosToken = []
    lsStopWordsComentarios = []

    for i in range(cantComentarios):
        # eliminación de numeros en comentarios

        texto = ''.join(c for c in unidecode.unidecode(lsComentarios[i][0]) if not c.isdigit())
        comentario = tokenizer.tokenize(texto.lower())
        comentarioTkn = nltk.pos_tag(comentario)
        comentario_sin_stop_words = [w for w in comentario if not w in stop_words]
        lsLimpiaComentarios.append(comentario)
        lsComentariosToken.append(comentarioTkn)
        longitudcomentario = len(comentario_sin_stop_words)
        if  longitudcomentario > 3:
            lsStopWordsComentarios.append(comentario_sin_stop_words)

    return lsStopWordsComentarios, lsComentariosToken, lsLimpiaComentarios;