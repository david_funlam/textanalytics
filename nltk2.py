# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 12:55:29 2019

@author: david.sanchez.b
"""
import nltk
# nltk.download ()
from nltk.corpus import brown
a = brown.words()


# remover cualquier tipo de puntuación con la libreria RegexpTokenizer y separa el texto para
# vectorizarlo

#Code Explanation:
#
#In this program, the objective was to remove all type of punctuations from given text. 
# We imported "RegexpTokenizer" which is a module of NLTK. It removes all the expression, symbol, character, numeric or any things whatever you want.
#You just have passed the regular Expression to the "RegexpTokenizer" module.
#Further, we tokenized the word using "tokenize" module. The output is stored in the "filterdText" variable.
#And printed them using "print()."

from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')
filterdText=tokenizer.tokenize('Hello Guru99, You have build a very good site and I love visiting your site.')
print(filterdText)  

###############################TOKENIZACION ##############################################################
# la tokenización es el proceso donde grandes cantidades de texto es divido en pequeñas partes llamadas TOKENS
# existen 2 submodulos para la tiokenización: word tokenize y sentence tokenize

# 1.  word_tokenize(): este metodo divide una sentencia o texto en palabras.
from nltk.tokenize import word_tokenize
text = "God is Great! I won a lottery."
print(word_tokenize(text))

#2. Tokenization of Sentences: es otro submodulo del NLTK. 
# Una pregunta obvia en su mente sería por qué se necesita la tokenización de oraciones cuando tenemos 
#la opción de la tokenización de palabras. 
# Imagine que necesita contar palabras promedio por oración, 
#¿cómo va a calcular? Para lograr tal tarea, necesita tanto la tokenización de oraciones como las palabras 
#para calcular la relación. Tal salida sirve como una característica importante para el entrenamiento 
# de la máquina, ya que la respuesta sería numérica.
from nltk.tokenize import sent_tokenize
text = "God is Great! I won a lottery."
############################### FIN TOKENIZACION ##############################################################

###############################POS (Part-Of-Speech) Tagging & Chunking with NLTK ##############################################################
# REFERENCIA: https://www.guru99.com/pos-tagging-chunking-nltk.html
# POS Tagging: El etiquetado de partes del discurso es responsable de leer el texto en un idioma y asignar un token específico (partes del discurso) a cada palabra.
# Chunking: |El fragmentación se utiliza para agregar más estructura a la oración siguiendo las etiquetas de las partes del discurso (POS). También se conoce como análisis superficial. El grupo resultante de palabras se llama "fragmentos". En el análisis superficial, existe un máximo de un nivel entre las raíces y las hojas, mientras que el análisis profundo comprende más de un nivel. El análisis superficial también se denomina análisis o fragmentación ligera.
# El uso principal de la fragmentación es hacer un grupo de "frases nominales". Las partes del discurso se combinan con expresiones regulares.
from nltk import pos_tag
from nltk import RegexpParser
text ="learn php from guru99 and make study easy".split()
print("After Split:",text)
# se le asigna la tokenbización a cada palabra de acuerdo a su naturaleza o sifnificado
tokens_tag = pos_tag(text)
print("After Token:",tokens_tag)

patterns= """mychunk:{<NN.?>*<VBD.?>*<JJ.?>*<CC>?}"""
chunker = RegexpParser(patterns)
print("After Regex:",chunker)
output = chunker.parse(tokens_tag)
print("After Chunking",output)

# ** Use Case of Chunking
# se usa para la detección de entidades. Una entidad es esa parte de la oración 
# por la cual la máquina obtiene el valor para cualquier intención
 import nltk
text = "learn php from guru99"
tokens = nltk.word_tokenize(text)
print(tokens)
tag = nltk.pos_tag(tokens)
print(tag)
grammar = "NP: {<DT>?<JJ>*<NN>}"
cp  =nltk.RegexpParser(grammar)
result = cp.parse(tag)
print(result)
result.draw()    # It will draw the pattern graphically which can be seen in Noun Phrase chunking 
#############################################################FIN##########################################

############################################################# Stemming ##########################################+
## QUE ES EL STEMMING: La derivación es una especie de normalización de las palabras. 
#La normalización es una técnica en la que un conjunto de palabras en una oración se convierte 
#en una secuencia para acortar su búsqueda. Las palabras que tienen el mismo significado pero que tienen alguna variación 
#según el contexto o la oración están normalizadas.
#En otra palabra, hay una palabra raíz, pero hay muchas variaciones de las mismas palabras. 
#Por ejemplo, la palabra raíz es "comer" y sus variaciones son "comer, comiendo, comido y así ". 
# De la misma manera, con la ayuda de Stemming, podemos encontrar la raíz de cualquier variación.

# Ejemplo
# He was riding.	
# He was taking the ride.

# En las dos oraciones anteriores, el significado es el mismo, es decir, la actividad de montar en el pasado. 
#Un humano puede comprender fácilmente que ambos significados son iguales. 
#Pero para las máquinas, ambas oraciones son diferentes. 
#Por lo tanto, se hizo difícil convertirlo en la misma fila de datos. 
#En caso de que no proporcionemos el mismo conjunto de datos, la máquina no puede predecir. 
#Por lo tanto, es necesario diferenciar el significado de cada palabra para preparar el conjunto de datos 
#para el aprendizaje automático. Y aquí la derivación se usa para clasificar el mismo tipo de datos 
#obteniendo su raíz.

#Implementemos esto con un programa Python.NLTK tiene un algoritmo llamado "PorterStemmer". 
#Este algoritmo acepta la lista de palabras tokenizadas y la deriva a la palabra raíz.

from nltk.stem import PorterStemmer
e_words= ["wait", "waiting", "waited", "waits"]
ps =PorterStemmer()
for w in e_words:
    rootWord=ps.stem(w)
    print(rootWord)
    
# en resumidas cuienta el stemming nos indica la palabra raiz, origen o base de un conjunto de palabras
############################################################# FIN Stemming ##########################################+  
  
############################################################# Lemmatization ##########################################+
# La lematización es el proceso algorítmico de encontrar el lema de una palabra dependiendo de su significado. 
# La lematización generalmente se refiere al análisis morfológico de las palabras, cuyo objetivo es eliminar 
# las terminaciones de inflexión. Ayuda a devolver la base o la forma del diccionario de una palabra, 
# que se conoce como lema. 
#El método de lematización NLTK se basa en la función de transformación incorporada de WorldNet. 
# El preprocesamiento de texto incluye tanto la derivación como la lematización. 
# Muchas personas encuentran los dos términos confusos. 
# Algunos tratan estos como iguales, pero hay una diferencia entre estos dos. 
#Se prefiere la lematización sobre la primera debido a la razón a continuación.    

# Stemming code
import nltk
from nltk.stem.porter import PorterStemmer
porter_stemmer  = PorterStemmer()
text = "studies studying cries cry"
tokenization = nltk.word_tokenize(text)
for w in tokenization:
    print("Stemming for {} is {}".format(w,porter_stemmer.stem(w))) 

# Lemmatization code
import nltk 
from nltk.stem import 	WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
text = "studies studying cries cry"
tokenization = nltk.word_tokenize(text)
for w in tokenization:
    print("Lemma for {} is {}".format(w, wordnet_lemmatizer.lemmatize(w)))  
    
# PARA EL CASO DE USO DE LA LEMMATIZACION VER: https://www.guru99.com/stemming-lemmatization-python-nltk.html
############################################################# FIN Lemmatization ##########################################+    
    
    
    
##################################################### WordNet with NLTK: Finding Synonyms for words in Python ######
from nltk.corpus import wordnet as guru
syns = guru.synsets("dog")
print(syns)
##################################################### FIN WordNet with NLTK: Finding Synonyms for words in Python ######

#################################################### Tagging Problems and Hidden Markov Model ###################
# Tagging Sentence en un sentido más amplio se refiere a la adición de etiquetas del verbo, sustantivo, etc.
# por el contexto de la oración. La identificación de etiquetas POS es un proceso complicado. 
# Por lo tanto, el etiquetado genérico de POS no es posible manualmente, ya que algunas palabras pueden tener 
# significados diferentes (ambiguos) de acuerdo con la estructura de la oración. 
# La conversión de texto en forma de lista es un paso importante antes de etiquetar ya que cada palabra 
# en la lista se enlaza y cuenta para una etiqueta en particular. 

# Consulte el siguiente código para comprenderlo mejor.
import nltk
text = "Hello Guru99, You have to build a very good site, and I love visiting your   site."
sentence = nltk.sent_tokenize(text)
for sent in sentence:
	 print(nltk.pos_tag(nltk.word_tokenize(sent)))
################################################ FIN Tagging Problems and Hidden Markov Model ###################
     

##################################### Counting POS Tags, Frequency Distribution & Collocations in NLTK ##########
# REVISAR ESTA REFERENCIA: https://www.guru99.com/counting-pos-tags-nltk.html

#EN ESTE EJEMPLO SE REALIZA LA TOKENIZACIÓN DE UN TEXTO Y COMO RESULTADO FINAL SE TIENE LA CANTIDAD DE VECES
# QUE EL TOKEN SE REPITIO
import nltk
from collections import Counter
text = " Guru99 is one of the best sites to learn WEB, SAP, Ethical Hacking and much more online."
lower_case = text.lower()
tokens = nltk.word_tokenize(lower_case)
tags = nltk.pos_tag(tokens)
counts = Counter( tag for word,  tag in tags)
print(counts)

#***************** Frequency Distribution
import nltk
a = "Guru99 is the site where you can find the best tutorials for Software Testing     Tutorial, SAP Course for Beginners. Java Tutorial for Beginners and much more. Please     visit the site guru99.com and much more."
words = nltk.tokenize.word_tokenize(a)
fd = nltk.FreqDist(words)
fd.plot()

def imprime():
    print('hola')


